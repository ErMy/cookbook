# Cookbook 
A website for creating, searching and storing recipes. Made with ASP.NET Core MVC and Entity Framework.

[Check out the website](https://ermy-cookbook.azurewebsites.net/)
