﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Areas.Identity.Data
{
    public class ApplicationUser : IdentityUser
    {
        public bool IsGuest { get; set; }
    }
}
