﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    /// <summary>
    /// Model for image files
    /// </summary>
    public class Image
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FileName { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public int RecipeId { get; set; }
    }
}
