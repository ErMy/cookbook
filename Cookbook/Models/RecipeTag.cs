﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    /// <summary>
    /// Assigns tag ids to recipe ids
    /// </summary>
    public class RecipeTag
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int RecipeId { get; set; }
        [Required]
        public int TagId { get; set; }
    }
}
