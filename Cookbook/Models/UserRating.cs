﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    /// <summary>
    /// Rating a user has given a recipe
    /// </summary>
    public class UserRating
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public int RecipeId { get; set; }
        [Required]
        public int Rating { get; set; }
    }
}
