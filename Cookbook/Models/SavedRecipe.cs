﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    /// <summary>
    /// Model for recipes a user has added to his cookbook
    /// </summary>
    public class SavedRecipe
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int RecipeId { get; set; }
        [Required]
        public string UserName { get; set; }
    }
}
