﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Models
{
    /// <summary>
    /// Model for recipe data
    /// </summary>
    public class Recipe
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(32)]
        public string Creator { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Ingredients are required")]
        public string Ingredients { get; set; }
        [Required(ErrorMessage = "Directions are required")]
        public string Directions { get; set; }
        [Required(ErrorMessage = "Time is required")]
        public int Time { get; set; }
        [Required(ErrorMessage = "Difficulty is required")]
        [Range(1,3, ErrorMessage = "Choose one of the given difficulty options")]
        public int Difficulty { get; set; }
        [Range(0, int.MaxValue)]
        public int OneStars { get; set; }
        [Range(0, int.MaxValue)]
        public int TwoStars { get; set; }
        [Range(0, int.MaxValue)]
        public int ThreeStars { get; set; }
        [Range(0, int.MaxValue)]
        public int FourStars { get; set; }
        [Range(0, int.MaxValue)]
        public int FiveStars { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime CreationDate { get; set; }

        public string[][] ParsedIngredients()
        {
            string[] parsed = Ingredients.Split('|');
            string[][] ingredientTable = new string[parsed.Length / 2][];

            for (int i = 0, j = 0; i < parsed.Length; i+=2, j++)
            {
                ingredientTable[j] = new string[2];
                ingredientTable[j][0] = parsed[i];
                ingredientTable[j][1] = parsed[i+1];
            }

            return ingredientTable;
        }

        public float CalculateRating()
        {
            return (OneStars + 2 * TwoStars + 3 * ThreeStars + 4 * FourStars + 5 * FiveStars) / MathF.Max(1, (OneStars + TwoStars + ThreeStars + FourStars + FiveStars));
        }

        public float CalculateRatingRounded()
        {
            return MathF.Round(CalculateRating() * 2, MidpointRounding.AwayFromZero) / 2;
        }

        public int GetRatingsCount()
        {
            return OneStars + TwoStars + ThreeStars + FourStars + FiveStars;
        }
    }

    public enum Difficulty
    {
        Simple = 1, Normal = 2, Intricate = 3
    }
}
