﻿using Cookbook.Areas.Identity.Data;
using Cookbook.Data;
using Cookbook.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Cookbook.Controllers
{

    public class RecipeController : Controller
    {
        private readonly ApplicationDbContext _db;
        private IWebHostEnvironment _environment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RecipeController> _logger;

        public RecipeController(ApplicationDbContext db, IWebHostEnvironment environment, UserManager<ApplicationUser> userManager, ILogger<RecipeController> logger)
        {
            _db = db;
            _environment = environment;
            _userManager = userManager;
            _logger = logger;
        }

        // Page for search results
        public async Task<IActionResult> Index(string sortOrder, string sortBy, string currentFilter, string searchString, int? pageNumber, FilterBy? filterBy)
        {
            // sort search results by rating or date
            // toggle asc, desc by using the same sortBy twice
            if (sortBy == "rating")
            {
                if (sortOrder == "rating_desc")
                {
                    sortOrder = "rating_asc";
                } else
                {
                    sortOrder = "rating_desc";
                }
            } else if (sortBy == "date")
            {
                if (sortOrder == "date_desc")
                {
                    sortOrder = "date_asc";
                } else
                {
                    sortOrder = "date_desc";
                }
            }

            // sort by rating desc if nothing selected
            if (string.IsNullOrEmpty(sortOrder))
            {
                sortOrder = "rating_desc";
            }

            ViewData["CurrentSort"] = sortOrder;
            ViewData["FilterBy"] = filterBy;

            // If the search string was changed, page has to be reset to 1
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            IQueryable<Recipe> recipes = _db.Recipe.AsNoTracking();

            // Filter results by search query
            if (!string.IsNullOrEmpty(searchString))
            {
                switch (filterBy)
                {
                    // get all recipes of creator
                    case FilterBy.UserName:
                        recipes = recipes.Where(x => x.Creator == searchString);
                        break;
                    // get all recipes with tag
                    case FilterBy.Tag:
                        List<int> tags2 = _db.Tag.AsNoTracking().Where(x => x.Title.Contains(searchString)).Select(x => x.Id).ToList();
                        List<int> recipeids2 = _db.RecipeTag.AsNoTracking().Where(x => tags2.Contains(x.TagId)).Select(x => x.RecipeId).ToList();
                        recipes = recipes.Where(x => recipeids2.Contains(x.Id));
                        break;
                    // get all recipes containing the search query in its ingredients, title or tags
                    default:
                        List<int> tags = _db.Tag.AsNoTracking().Where(x => x.Title.Contains(searchString)).Select(x => x.Id).ToList();
                        List<int> recipeids = _db.RecipeTag.AsNoTracking().Where(x => tags.Contains(x.TagId)).Select(x => x.RecipeId).ToList();
                        recipes = recipes.Where(x => x.Title.Contains(searchString) || x.Ingredients.Contains(searchString) || recipeids.Contains(x.Id));
                        break;
                }
            }

            // do the actual sorting
            switch (sortOrder)
            {
                case "date_asc":
                    recipes = recipes.OrderBy(x => x.CreationDate);
                    break;
                case "date_desc":
                    recipes = recipes.OrderByDescending(x => x.CreationDate);
                    break;
                case "rating_asc":
                    recipes = recipes.OrderBy(x => (x.OneStars + 2 * x.TwoStars + 3 * x.ThreeStars + 4 * x.FourStars + 5 * x.FiveStars) / ((x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars) == 0 ? 1f : (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)));
                    break;
                default:
                    recipes = recipes.OrderByDescending(x => (x.OneStars + 2 * x.TwoStars + 3 * x.ThreeStars + 4 * x.FourStars + 5 * x.FiveStars) / ((x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars) == 0 ? 1f : (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)));
                    break;
            }

            // get all recipes that will be displayed on this page
            int pageSize = 20;
            PaginatedList<Recipe> recipeList = await PaginatedList<Recipe>.CreateAsync(recipes.AsNoTracking(), pageNumber ?? 1, pageSize);
            List<string> imageFiles = new List<string>();
            List<string>[] recipeTags = new List<string>[recipeList.Count];

            // get thumbnails and tags of recipes on this page
            for (int i = 0; i < recipeList.Count; i++)
            {
                var image = _db.Image.AsNoTracking().Where(x => x.RecipeId == recipeList[i].Id).FirstOrDefault();
                imageFiles.Add(image == null? "" : image.FileName);

                recipeTags[i] = GetTagsOfRecipe(recipeList[i].Id);
            }

            ViewData["Images"] = imageFiles;
            ViewData["Tags"] = recipeTags;

            return View(recipeList);
        }

        // Redirects to List page showing recipes with given tag
        public IActionResult GetRecipesWithTag(string tag)
        {

            return RedirectToAction("Index", new { searchString = tag, filterBy = FilterBy.Tag });
        }

        // Redirects to List page showing recipes with given creator
        public IActionResult GetRecipesFromCreator(string creator)
        {
            return RedirectToAction("Index", new { searchString = creator, filterBy = FilterBy.UserName });
        }

        [Authorize]
        // Personal cookbook page. Contains recipes uploaded by user and saved recipes.
        public async Task<IActionResult> Cookbook(string sortOrder, string sortBy, string currentFilter, string searchString, int? pageNumber, string filterBy)
        {
            // decide sortorder
            // toggle asc, desc by using the same sortBy twice
            if (sortBy == "rating")
            {
                if (sortOrder == "rating_desc")
                {
                    sortOrder = "rating_asc";
                }
                else
                {
                    sortOrder = "rating_desc";
                }
            }
            else if (sortBy == "date")
            {
                if (sortOrder == "date_desc")
                {
                    sortOrder = "date_asc";
                }
                else
                {
                    sortOrder = "date_desc";
                }
            }

            // sort by rating desc if nothing selected
            if (string.IsNullOrEmpty(sortOrder))
            {
                sortOrder = "rating_desc";
            }

            ViewData["CurrentSort"] = sortOrder;
            ViewData["FilterBy"] = filterBy;

            // If the search string was changed, page has to be reset to 1
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            IQueryable<Recipe> recipes = null;

            // filter recipes by categories
            switch (filterBy)
            {
                case "saved":
                    var saved = _db.SavedRecipe.AsNoTracking().Where(x => x.UserName == User.Identity.Name).Select(x => x.RecipeId);
                    recipes = _db.Recipe.AsNoTracking().Where(x => saved.Contains(x.Id));
                    break;
                case "created":
                    recipes = _db.Recipe.AsNoTracking().Where(x => x.Creator == User.Identity.Name);
                    break;
                // all recipes
                default:
                    var save = _db.SavedRecipe.AsNoTracking().Where(x => x.UserName == User.Identity.Name).Select(x => x.RecipeId);
                    recipes = _db.Recipe.AsNoTracking().Where(x => save.Contains(x.Id) || x.Creator == User.Identity.Name);
                    break;
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                // filter by searchstring
                var tags = _db.Tag.AsNoTracking().Where(x => x.Title.Contains(searchString)).Select(x => x.Id);
                var recipeids = _db.RecipeTag.AsNoTracking().Where(x => tags.Contains(x.TagId)).Select(x => x.RecipeId);
                recipes = recipes.AsNoTracking().Where(x => x.Title.Contains(searchString) || x.Ingredients.Contains(searchString) || recipeids.Contains(x.Id));
            }

            // do the actual sorting
            switch (sortOrder)
            {
                case "date_asc":
                    recipes = recipes.OrderBy(x => x.CreationDate);
                    break;
                case "date_desc":
                    recipes = recipes.OrderByDescending(x => x.CreationDate);
                    break;
                case "rating_asc":
                    recipes = recipes.OrderBy(x => (x.OneStars + 2 * x.TwoStars + 3 * x.ThreeStars + 4 * x.FourStars + 5 * x.FiveStars) / ((x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars) == 0 ? 1f : (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)));
                    break;
                default:
                    recipes = recipes.OrderByDescending(x => (x.OneStars + 2 * x.TwoStars + 3 * x.ThreeStars + 4 * x.FourStars + 5 * x.FiveStars) / ((x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars) == 0 ? 1f : (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)));
                    break;
            }

            // get all recipes that will be displayed on this page
            int pageSize = 20;
            PaginatedList<Recipe> recipeList = await PaginatedList<Recipe>.CreateAsync(recipes.AsNoTracking(), pageNumber ?? 1, pageSize);
            List<string> imageFiles = new List<string>();
            List<string>[] recipeTags = new List<string>[recipeList.Count];

            // get thumbnail and tags for each recipe on this page
            for (int i = 0; i < recipeList.Count; i++)
            {
                var image = _db.Image.AsNoTracking().Where(x => x.RecipeId == recipeList[i].Id).FirstOrDefault();
                imageFiles.Add(image == null ? "" : image.FileName);
                recipeTags[i] = GetTagsOfRecipe(recipeList[i].Id);
            }

            ViewData["Images"] = imageFiles;
            ViewData["Tags"] = recipeTags;


            return View(recipeList);
        }

        [Authorize]
        // Page for creating a new recipe
        public IActionResult Create()
        {
            return View();
        }

        // Adds recipe to database if no errors
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Recipe obj, [FromForm] string tags, [FromForm] IFormFileCollection files)
        {
            obj.Ingredients = obj.Ingredients.TrimEnd('|');
            if (string.IsNullOrWhiteSpace(obj.Ingredients))
            {
                ModelState.AddModelError(nameof(obj.Ingredients), "Ingredients are required");
            }

            if (files.Count > 0 && files.Any(file => !ExtensionCheck.IsValidImage(file)))
            {
                ModelState.AddModelError("", "Only .jpg and .png files are supported.");
            }

            var user = await _userManager.GetUserAsync(User);

            if (user.IsGuest)
            {
                ModelState.AddModelError("", "Recipes cannot be created on guest accounts.");
            }

            if (ModelState.IsValid)
            {
                // make sure the table has an even amount of entries
                if (obj.Ingredients.Split('|').Length % 2 > 0)
                {
                    obj.Ingredients += '|';
                }

                obj.CreationDate = DateTime.Now;
                obj.Creator = HttpContext.User.Identity.Name;
                _db.Recipe.Add(obj);
                await _db.SaveChangesAsync();

                List<Image> images = new List<Image>();

                // add images to database
                foreach (var image in files)
                {
                    if (image != null && image.Length > 0)
                    {
                        IFormFile file = image;
                        string uploads = Path.Combine(_environment.WebRootPath, "uploads", "img", "recipes");

                        if (!Directory.Exists(uploads))
                        {
                            Directory.CreateDirectory(uploads);
                        }

                        if (file.Length > 0)
                        {
                            string fileName = $"{Guid.NewGuid()}{new FileInfo(file.FileName).Extension}";

                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                images.Add(new Image() { FileName = fileName, RecipeId = obj.Id, UserName = User.Identity.Name});
                            }
                        }
                    }
                }

                _db.Image.AddRange(images);

                // Add tags
                if (!string.IsNullOrWhiteSpace(tags))
                {

                    foreach (string tag in tags.Split('|'))
                    {
                        if (!string.IsNullOrWhiteSpace(tag))
                        {
                            // Check if tag title exists
                            Tag newTag = _db.Tag.Where(x => x.Title == tag).FirstOrDefault();
                            if (newTag != null)
                            {
                                RecipeTag recipeTag = new RecipeTag() { RecipeId = obj.Id, TagId = newTag.Id };
                                _db.RecipeTag.Add(recipeTag);
                            }
                        }
                    }
                }

                await _db.SaveChangesAsync();

                // redirect to the page of the recipe just created
                return RedirectToAction("Details", new { id = obj.Id });
            }

            return View();
        }

        // Page containing all information of a recipe
        public IActionResult Details(int? id, string status = "")
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            Recipe obj = _db.Recipe.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            // Get all tags belonging to this recipe
            List<string> tags = new List<string>();
            List<int> tagids = _db.RecipeTag.AsNoTracking().Where(x => x.RecipeId == obj.Id).Select(x => x.TagId).ToList();
            foreach (int tagid in tagids)
            {
               Tag tag = _db.Tag.Find(tagid);
                if (tag != null)
                {
                    tags.Add(tag.Title);
                }
            }

            // Get all images belonging to this recipe
            List<string> images = _db.Image.AsNoTracking().Where(x => x.RecipeId == obj.Id).Select(f => f.FileName).ToList();
            // Get the rating this user gave this recipe if available
            UserRating userRating = _db.UserRating.AsNoTracking().Where(x => x.RecipeId == obj.Id && x.UserName == User.Identity.Name).FirstOrDefault();

            ViewData["Images"] = images;
            ViewData["Tags"] = tags;
            ViewData["UserRating"] = userRating == null ? 0 : userRating.Rating;
            // Has the user saved this recipe already?
            ViewData["Saved"] = _db.SavedRecipe.AsNoTracking().Where(x => x.UserName == User.Identity.Name && x.RecipeId == obj.Id).FirstOrDefault() != null;
            ViewData["Status"] = status;

            return View(obj);
        }

        
        // Edit page of a recipe
        [Authorize]
        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            Recipe obj = _db.Recipe.Find(id);
            if (obj == null || obj.Creator != User.Identity.Name)
            {
                return NotFound();
            }

            ViewData["Tags"] = GetTagsOfRecipe(obj.Id);

            return View(obj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(Recipe obj, [FromForm] string tags)
        {

            Recipe oldObj = _db.Recipe.Where(x => x.Id == obj.Id).FirstOrDefault();
            if (oldObj != null && oldObj.Creator == User.Identity.Name)
            {
                obj.OneStars = oldObj.OneStars;
                obj.TwoStars = oldObj.TwoStars;
                obj.ThreeStars = oldObj.ThreeStars;
                obj.FourStars = oldObj.FourStars;
                obj.FiveStars = oldObj.FiveStars;
            } else
            {
                ModelState.AddModelError("", "Data is invalid");
            }

            // Check if ingredients is empty
            obj.Ingredients = obj.Ingredients.TrimEnd('|');
            if (string.IsNullOrWhiteSpace(obj.Ingredients))
            {
                ModelState.AddModelError(nameof(obj.Ingredients), "Ingredients are required");
            }

            ApplicationUser user = await _userManager.GetUserAsync(User);

            if (user.IsGuest)
            {
                ModelState.AddModelError("", "Recipes cannot be edited on guest accounts.");
            }

            // Server side validation. ApplicationType uses client side validation.
            if (ModelState.IsValid)
            {
                if (obj.Ingredients.Split('|').Length % 2 > 0)
                {
                    obj.Ingredients += '|';
                }

                _db.Recipe.Update(obj);

                // Remove old tags
                _db.RecipeTag.RemoveRange(_db.RecipeTag.Where(x => x.RecipeId == obj.Id));

                // Add new tags
                if (!string.IsNullOrWhiteSpace(tags))
                {
                    foreach (string tag in tags.Split('|'))
                    {
                        if (!string.IsNullOrWhiteSpace(tag))
                        {
                            Tag newTag = _db.Tag.Where(x => x.Title == tag).FirstOrDefault();
                            if (newTag != null)
                            {
                                RecipeTag recipeTag = new RecipeTag() { RecipeId = obj.Id, TagId = newTag.Id };
                                _db.RecipeTag.Add(recipeTag);
                            }
                        }
                    }
                }

                _db.SaveChanges();
                return RedirectToAction("Details", new { id = obj.Id });
            }

            return View(obj);
        }

        [Authorize]
        // Delete recipe
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            Recipe obj = _db.Recipe.Find(id);
            if (obj == null || obj.Creator != User.Identity.Name)
            {
                return NotFound();
            }

            ApplicationUser user = await _userManager.GetUserAsync(User);

            if (user.IsGuest)
            {
                return RedirectToAction("Details", new { id = obj.Id, status = "Recipes cannot be deleted on guest accounts." });
            }

            foreach (RecipeTag item in _db.RecipeTag.Where(x => x.RecipeId == obj.Id))
            {
                _db.RecipeTag.Remove(item);
            }

            foreach (SavedRecipe item in _db.SavedRecipe.Where(x => x.RecipeId == obj.Id))
            {
                _db.SavedRecipe.Remove(item);
            }

            foreach (Image img in _db.Image.Where(x => x.RecipeId == obj.Id))
            {
                System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", "img", "recipes", img.FileName));
                _db.Image.Remove(img);
            }

            foreach (UserRating rating in _db.UserRating.Where(x => x.RecipeId == obj.Id))
            {
                _db.UserRating.Remove(rating);
            }

            _db.Recipe.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Cookbook");
        }

        // Page for adding images to a recipe
        [Authorize]
        public IActionResult UploadImage(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            return View(id);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UploadImage(int? id, IFormFileCollection files)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            if (files.Count > 0 && files.Any(file => !ExtensionCheck.IsValidImage(file)))
            {
                ModelState.AddModelError("", "Only .jpg and .png files are supported.");
            }

            if (ModelState.IsValid)
            {
                List<Image> images = new List<Image>();

                foreach (IFormFile image in files)
                {
                    if (image != null && image.Length > 0)
                    {
                        IFormFile file = image;
                        string uploads = Path.Combine(_environment.WebRootPath, "uploads\\img\\recipes");

                        if (!Directory.Exists(uploads))
                        {
                            Directory.CreateDirectory(uploads);
                        }

                        if (file.Length > 0)
                        {
                            string fileName = $"{Guid.NewGuid()}{new FileInfo(file.FileName).Extension}";

                            using (FileStream fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                images.Add(new Image() { FileName = fileName, RecipeId = id.Value, UserName = User.Identity.Name });
                            }
                        }
                    }
                }

                _db.Image.AddRange(images);

                await _db.SaveChangesAsync();

                return RedirectToAction("Details", new { id = id.Value });
            }

            return View();
        }

        // Page containing all images belonging to a recipe
        public async Task<IActionResult> Gallery(int? id, int? pageNumber)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            Recipe obj = _db.Recipe.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            int pageSize = 24;

            ViewData["RecipeId"] = obj.Id;

            return View(await PaginatedList<Image>.CreateAsync(_db.Image.AsNoTracking().Where(x => x.RecipeId == obj.Id), pageNumber ?? 1, pageSize));
        }

        [Authorize]
        // Page containing all images a user has uploaded
        public async Task<IActionResult> UserGallery(int? pageNumber, string status = "")
        {
            int pageSize = 24;

            ViewData["Status"] = status;

            return View(await PaginatedList<Image>.CreateAsync(_db.Image.AsNoTracking().Where(x => x.UserName == User.Identity.Name), pageNumber ?? 1, pageSize));
        }

        // Delete image uploaded by this user
        public async Task<IActionResult> DeleteImage(int? imageId)
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);

            if (user.IsGuest)
            {
                return RedirectToAction("UserGallery", new { status = "Images cannot be deleted on guest accounts." });
            }

            if (imageId != null)
            {
                Image img = _db.Image.Find(imageId);

                if (img != null)
                {
                    if (img.UserName == User.Identity.Name)
                    {
                        System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", "img", "recipes", img.FileName));
                        _db.Image.Remove(img);
                        _db.SaveChanges();
                    }
                }
            }

            return RedirectToAction("UserGallery");
        }

        [HttpGet]
        // Ajax - Get ALL tags in the database
        public string[] GetAllTags()
        {
            return _db.Tag.Select(x => x.Title).ToArray();
        }

        // Helper - Return tags belonging to this recipe
        public List<string> GetTagsOfRecipe(int id)
        {
            Recipe obj = _db.Recipe.Find(id);
            if (obj == null)
            {
                return new List<string>();
            }

            List<int> recipetags = _db.RecipeTag.AsNoTracking().Where(x => x.RecipeId == obj.Id).Select(x => x.TagId).ToList();
            List<string> tags = new List<string>();
            foreach (int tagid in recipetags)
            {
                Tag tag = _db.Tag.Find(tagid);
                if (tag != null)
                {
                    tags.Add(tag.Title);
                }
            }

            return tags;
        }

        [HttpPost]
        [Authorize]
        // Ajax - Rate recipe
        public bool RateRecipe(int rating, int recipeId)
        {
            // Get already existing rating if available
            UserRating entry = _db.UserRating.Where(x => x.RecipeId == recipeId && x.UserName == User.Identity.Name).FirstOrDefault();
            Recipe recipe = _db.Recipe.Where(x => x.Id == recipeId).FirstOrDefault();

            if (recipe == null || rating < 1 || rating > 5)
                return false;

            if (entry != null)
            {
                // remove old rating
                switch (entry.Rating)
                {
                    case 1:
                        recipe.OneStars = Math.Max(0, recipe.OneStars - 1);
                        break;
                    case 2:
                        recipe.TwoStars = Math.Max(0, recipe.TwoStars - 1);
                        break;
                    case 3:
                        recipe.ThreeStars = Math.Max(0, recipe.ThreeStars - 1);
                        break;
                    case 4:
                        recipe.FourStars = Math.Max(0, recipe.FourStars - 1);
                        break;
                    case 5:
                        recipe.FiveStars = Math.Max(0, recipe.FiveStars - 1);
                        break;
                }

                // same rating = remove old rating and no new rating
                if (entry.Rating == rating)
                {
                    _db.UserRating.Remove(entry);
                    _db.SaveChanges();
                    return false;
                }

                entry.Rating = rating;
                _db.UserRating.Update(entry);
            } else
            {
                _db.UserRating.Add(new UserRating() { Rating = rating, RecipeId = recipeId, UserName = User.Identity.Name });
            }

            // add new rating
            switch (rating)
            {
                case 1:
                    recipe.OneStars += 1;
                    break;
                case 2:
                    recipe.TwoStars += 1;
                    break;
                case 3:
                    recipe.ThreeStars += 1;
                    break;
                case 4:
                    recipe.FourStars += 1;
                    break;
                case 5:
                    recipe.FiveStars += 1;
                    break;
            }

            _db.Recipe.Update(recipe);

            _db.SaveChanges();

            return true;
        }

        [HttpPost]
        [Authorize]
        // Ajax -  Adds recipe to cookbook of user
        public bool SaveRecipe(int recipeId)
        {
            SavedRecipe entry = _db.SavedRecipe.AsNoTracking().Where(x => x.RecipeId == recipeId && x.UserName == User.Identity.Name).FirstOrDefault();
            if (entry == null)
            {
                // Create entry
                _db.SavedRecipe.Add(new SavedRecipe() { RecipeId = recipeId, UserName = User.Identity.Name });
                _db.SaveChanges();
                return true;
            } else
            {
                // Remove entry
                _db.SavedRecipe.Remove(entry);
                _db.SaveChanges();
                return false;
            }
        }
    }

    public enum FilterBy
    {
        SearchString = 1, UserName = 2, Tag = 3
    }
}
