﻿using Cookbook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook.Services
{
    public class DailyRecipesService
    {
        public Recipe recipeOfTheDay { get; set; }
        public string recipeOfTheDayImg { get; set; }
        public List<Recipe> topRecipes { get; set; }
        public List<string> topRecipesImg { get; set; }
        public List<Recipe> popularRecipes { get; set; }
        public List<string> popularRecipesImg { get; set; }
    }
}
