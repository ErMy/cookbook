﻿using Cookbook.Models;
using System.Threading.Tasks;

namespace Cookbook.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
        Task SendWelcomeEmailAsync(string userName, string callbackUrl, string subject, string toEmail);
    }
}
