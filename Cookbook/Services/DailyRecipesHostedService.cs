﻿using Cookbook.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cookbook.Services
{
    public class DailyRecipesHostedService : BackgroundService
    {
        protected IServiceProvider _serviceProvider;
        protected DailyRecipesService _dailyRecipes;

        public DailyRecipesHostedService([NotNull] IServiceProvider serviceProvider, DailyRecipesService dailyRecipesService)
        {
            _serviceProvider = serviceProvider;
            _dailyRecipes = dailyRecipesService;
        }

        /// <summary>
        /// Every day, select a recipe of the day and update top/popular recipes
        /// </summary>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //_dailyRecipes.popularRecipes = new List<Models.Recipe>();
            //_dailyRecipes.popularRecipesImg = new List<string>();
            //_dailyRecipes.recipeOfTheDay = null;
            //_dailyRecipes.recipeOfTheDayImg = "";
            //_dailyRecipes.topRecipes = new List<Models.Recipe>();
            //_dailyRecipes.topRecipesImg = new List<string>();
            //return;

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var appDbContext = (ApplicationDbContext)scope.ServiceProvider.GetRequiredService(typeof(ApplicationDbContext));
                    Random r = new Random();

                    int count = appDbContext.Recipe.Count();

                    if (count == 0)
                    {
                        break;
                    }
                    else
                    {
                        _dailyRecipes.recipeOfTheDay = appDbContext.Recipe.ToList()[r.Next(1, count)];
                    }

                    Models.Image dayImg = appDbContext.Image.Where(x => x.RecipeId == _dailyRecipes.recipeOfTheDay.Id).FirstOrDefault();
                    _dailyRecipes.recipeOfTheDayImg = dayImg == null ? "" : dayImg.FileName;

                    List<Models.Recipe> topRecipes = appDbContext.Recipe.OrderByDescending(x => (x.OneStars + 2 * x.TwoStars + 3 * x.ThreeStars + 4 * x.FourStars + 5 * x.FiveStars) 
                    / ((x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars) == 0 ? 1f : (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)))
                        .Take(15).ToList();
                    _dailyRecipes.topRecipes = topRecipes;
                    List<string> topImages = new List<string>();
                    foreach (var item in topRecipes)
                    {
                        Models.Image image = appDbContext.Image.Where(x => x.RecipeId == item.Id).FirstOrDefault();
                        topImages.Add(image == null ? "" : image.FileName);
                    }
                    _dailyRecipes.topRecipesImg = topImages;

                    List<Models.Recipe> popularRecipes = appDbContext.Recipe.OrderByDescending(x => (x.OneStars + x.TwoStars + x.ThreeStars + x.FourStars + x.FiveStars)).Take(15).ToList();
                    _dailyRecipes.popularRecipes = popularRecipes;
                    List<string> popularImages = new List<string>();
                    foreach (var item in popularRecipes)
                    {
                        Models.Image image = appDbContext.Image.Where(x => x.RecipeId == item.Id).FirstOrDefault();
                        popularImages.Add(image == null ? "" : image.FileName);
                    }
                    _dailyRecipes.popularRecipesImg = popularImages;
                }

                await Task.Delay(new TimeSpan(24, 0, 0));
            }
        }
    }
}
