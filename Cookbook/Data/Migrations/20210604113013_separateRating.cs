﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cookbook.Data.Migrations
{
    public partial class separateRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Recipe");

            migrationBuilder.AddColumn<int>(
                name: "FiveStars",
                table: "Recipe",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FourStars",
                table: "Recipe",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OneStars",
                table: "Recipe",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ThreeStars",
                table: "Recipe",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TwoStars",
                table: "Recipe",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FiveStars",
                table: "Recipe");

            migrationBuilder.DropColumn(
                name: "FourStars",
                table: "Recipe");

            migrationBuilder.DropColumn(
                name: "OneStars",
                table: "Recipe");

            migrationBuilder.DropColumn(
                name: "ThreeStars",
                table: "Recipe");

            migrationBuilder.DropColumn(
                name: "TwoStars",
                table: "Recipe");

            migrationBuilder.AddColumn<decimal>(
                name: "Rating",
                table: "Recipe",
                type: "decimal(18,2)",
                nullable: true);
        }
    }
}
