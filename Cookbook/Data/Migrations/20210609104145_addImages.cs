﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cookbook.Data.Migrations
{
    public partial class addImages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Username",
                table: "UserRating");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UserRating",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserRating");

            migrationBuilder.AddColumn<string>(
                name: "Username",
                table: "UserRating",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
