﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cookbook.Data.Migrations
{
    public partial class changeUserIdToName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserRating");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Image");

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "UserRating",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Image",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "UserRating");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Image");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UserRating",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Image",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
