﻿using Cookbook.Areas.Identity.Data;
using Cookbook.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cookbook.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Recipe> Recipe { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<RecipeTag> RecipeTag { get; set; }
        public DbSet<UserRating> UserRating { get; set; }
        public DbSet<Image> Image { get; set; }
        public DbSet<SavedRecipe> SavedRecipe { get; set; }
    }
}
