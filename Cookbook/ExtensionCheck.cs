﻿using Microsoft.AspNetCore.Http;
using Myrmec;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cookbook
{
    public class ExtensionCheck
    {
        /// <summary>
        /// Check if uploaded images are valid files
        /// </summary>
        public static bool IsValidImage(IFormFile file)
        {
            var sniffer = new Sniffer();
            var supportedFiles = new List<Record>
            {
                new Record("jpg,jpeg", "ff,d8,ff,db"),
                new Record("jpg,jpeg", "ff,d8,ff,ee"),
                new Record("jpg,jpeg","FF D8 FF E0 ?? ?? 4A 46 49 46 00 01"),
                new Record("jpg,jpeg","FF D8 FF E1 ?? ?? 45 78 69 66 00 00"),
                new Record("png", "89,50,4e,47,0d,0a,1a,0a"),
            };
            sniffer.Populate(supportedFiles);

            byte[] fileHead = ReadFileHead(file);
            return sniffer.Match(fileHead).Count > 0;
        }

        private static byte[] ReadFileHead(IFormFile file)
        {
            using var fs = new BinaryReader(file.OpenReadStream());
            var bytes = new byte[20];
            fs.Read(bytes, 0, 20);
            return bytes;
        }
    }
}
